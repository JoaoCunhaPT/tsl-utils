Prerequisites for compiling this project
========================================

One of the dependencies for this project is
http://code.google.com/p/eid-tsl/ As the library is not available on
any maven repositories, you have to either manually install the
library to your local maven repository:

mvn install:install-file -Dfile=lib/eid-tsl-core-1.0.0-SNAPSHOT.jar 
-DgroupId=be.fedict.eid -Dversion=1.0.0-SNAPSHOT -DartifactId=eid-tsl-core -Dpackaging=jar

or refer the library explicitely from the pom.xml, using:

<dependency>
    <groupId>be.fedict.eid</groupId>
    <artifactId>eid-tsl-core</artifactId>
    <version>1.0.0-SNAPSHOT</version>
    <scope>system</scope>
    <systemPath>${basedir}/lib/eid-tsl-core-1.0.0-SNAPSHOT.jar</systemPath>
</dependency>


Running the TSLEditor
=====================

In order to run just point to the target and run java -jar
TSLEditor_Version.jar. If you want to copy the executable don't forget
to get also the lib folder.
